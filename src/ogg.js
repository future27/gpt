import { rejects } from "assert";
import axios from "axios";
import { createWriteStream } from "fs";
import {dirname, resolve} from "path";
import {fileURLToPath} from "url";
import Ffmpeg from "fluent-ffmpeg";
import installer from "@ffmpeg-installer/ffmpeg";
import { removeFile } from "./utils.js";

const __dirname = dirname(fileURLToPath(import.meta.url));

class OggConverter {
    constructor() {
        Ffmpeg.setFfmpegPath(installer.path)
    }

    toMp3(input, outPut) {
        try {
            const outPutPath = resolve(dirname(input), `${outPut}.mp3`); // путь до voices
            return new Promise((resolve, reject) => {
                Ffmpeg(input)
                    .inputOption('-t 30')
                    .output(outPutPath)
                    .on('end', () => {
                        removeFile(input);
                        resolve(outPutPath);
                    })
                    .on('error', err => reject(err.message))
                    .run()
            })

        } catch (e) {
            console.log(`Error while createing mp3: ${e.message}`);
        }
    }

    async create(url, filename) {
        try {
            const oggPath = resolve(__dirname, "../voices", `${filename}.ogg`)
            const response = await axios({
                method: 'get',
                url,
                responseType: 'stream',
            })
            return new Promise(resolve => {
                const stream = createWriteStream(oggPath);
                response.data.pipe(stream);
                stream.on("finish", () => resolve(oggPath))
            })
        } catch (e) {
            console.log(`Error while creating ogg: ${e.message}`)
        }
    }
}

export const ogg = new OggConverter();
